package com.yasinkacmaz.backgroundlocationtracker.utils.extension

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

fun Context.isLocationPermissionGranted(): Boolean {
    val isFineLocationGranted = ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION).isGranted()
    val isCoarseLocationGranted = ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION).isGranted()

    return isFineLocationGranted && isCoarseLocationGranted
}

private fun Int.isGranted() = this == PackageManager.PERMISSION_GRANTED
