package com.yasinkacmaz.backgroundlocationtracker.utils.extension

import kotlin.LazyThreadSafetyMode.NONE

fun <T> unsafeLazy(initializer: () -> T) = lazy(NONE, initializer)
