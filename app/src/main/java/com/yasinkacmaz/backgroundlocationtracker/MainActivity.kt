package com.yasinkacmaz.backgroundlocationtracker

import android.Manifest
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.animation.AlphaAnimation
import android.view.animation.Animation.REVERSE
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.yasinkacmaz.backgroundlocationtracker.utils.extension.unsafeLazy
import com.yasinkacmaz.backgroundlocationtracker.receiver.request.lastlocation.LastLocationRequestReceiver
import com.yasinkacmaz.backgroundlocationtracker.receiver.request.servicestatus.ServiceStatusRequestReceiver
import com.yasinkacmaz.backgroundlocationtracker.receiver.response.lastlocation.LastLocationResponseCallback
import com.yasinkacmaz.backgroundlocationtracker.receiver.response.lastlocation.LastLocationResponseReceiver
import com.yasinkacmaz.backgroundlocationtracker.receiver.response.servicestatus.ServiceStatusResponseCallback
import com.yasinkacmaz.backgroundlocationtracker.receiver.response.servicestatus.ServiceStatusResponseReceiver
import com.yasinkacmaz.backgroundlocationtracker.utils.extension.isLocationPermissionGranted
import kotlinx.android.synthetic.main.activity_main.checkServiceStatusButton
import kotlinx.android.synthetic.main.activity_main.getLastLocationTimeButton
import kotlinx.android.synthetic.main.activity_main.lastLocationDateTextView
import kotlinx.android.synthetic.main.activity_main.permissionStatusTextView
import kotlinx.android.synthetic.main.activity_main.requestPermissionButton
import kotlinx.android.synthetic.main.activity_main.serviceStatusImageView
import kotlinx.android.synthetic.main.activity_main.serviceStatusTextView
import kotlinx.android.synthetic.main.activity_main.startServiceButton
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class MainActivity : AppCompatActivity(), ServiceStatusResponseCallback,
    LastLocationResponseCallback {

    private val serviceStatusResponseReceiver by unsafeLazy { ServiceStatusResponseReceiver(this) }
    private val lastLocationResponseReceiver by unsafeLazy { LastLocationResponseReceiver(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkLocationPermission()

        checkServiceStatusButton.setOnClickListener {
            requestServiceStatus()
        }

        getLastLocationTimeButton.setOnClickListener {
            LocalBroadcastManager.getInstance(this)
                .sendBroadcast(LastLocationRequestReceiver.getIntent())
        }

        startServiceButton.setOnClickListener {
            startLocationTrackerService()
        }

        requestPermissionButton.setOnClickListener {
            checkLocationPermission()
        }
    }

    private fun requestServiceStatus() {
        renderServiceStatus(false)
        LocalBroadcastManager.getInstance(this)
            .sendBroadcast(ServiceStatusRequestReceiver.getIntent())
    }

    override fun onResume() {
        registerServiceStatusResponseReceiver()
        registerLastLocationResponseReceiver()
        requestServiceStatus()
        super.onResume()
    }

    override fun onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(serviceStatusResponseReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(lastLocationResponseReceiver)
        super.onStop()
    }

    private fun registerServiceStatusResponseReceiver() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                serviceStatusResponseReceiver,
                ServiceStatusResponseReceiver.getIntentFilter()
            )
    }

    private fun registerLastLocationResponseReceiver() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                lastLocationResponseReceiver,
                LastLocationResponseReceiver.getIntentFilter()
            )
    }

    private fun startLocationTrackerService() {
        startService(Intent(this, LocationTrackerService::class.java))
    }

    override fun onLastLocation(timeStamp: Long) {
        val dateFormat = SimpleDateFormat("dd MMMM YYYY HH:mm:ss", Locale.getDefault())
        val date = Date(timeStamp)
        val lastLocationDate = dateFormat.format(date)
        lastLocationDateTextView.text =
            getString(R.string.last_location_time, lastLocationDate)
        val alphaAnimation = AlphaAnimation(0.1f, 1.0f).apply {
            duration = 1500L
        }
        lastLocationDateTextView.startAnimation(alphaAnimation)
    }

    override fun onServiceStatusResponse() {
        renderServiceStatus(true)
    }

    private fun renderServiceStatus(isRunning: Boolean) {
        val textResId = if (isRunning) {
            R.string.location_service_is_running
        } else {
            R.string.location_service_is_not_running
        }

        val iconResId = if (isRunning) {
            R.drawable.ic_power_on_24dp
        } else {
            R.drawable.ic_power_off_24dp
        }

        serviceStatusTextView.setText(textResId)
        serviceStatusImageView.setImageResource(iconResId)
    }

    private fun checkLocationPermission() {
        if (!isLocationPermissionGranted()) {
            permissionStatusTextView.setText(R.string.has_no_permission)
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_FINE_LOCATION)) {
                showExplanation()
            } else {
                requestPermission()
            }
        } else {
            permissionStatusTextView.setText(R.string.has_permission)
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(ACCESS_FINE_LOCATION),
            PERMISSION_REQUEST_CODE
        )
    }

    private fun showExplanation() {
        AlertDialog
            .Builder(this)
            .setIcon(R.drawable.ic_location_on_black_24dp)
            .setMessage(R.string.permission_rationale)
            .setPositiveButton(android.R.string.ok) { _, _ -> requestPermission() }
            .create()
            .show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            checkLocationPermission()
        }
    }

    companion object {
        private const val PERMISSION_REQUEST_CODE = 13
    }
}
