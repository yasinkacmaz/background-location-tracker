package com.yasinkacmaz.backgroundlocationtracker

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Binder
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MAX
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.yasinkacmaz.backgroundlocationtracker.receiver.request.lastlocation.LastLocationRequestCallback
import com.yasinkacmaz.backgroundlocationtracker.receiver.request.lastlocation.LastLocationRequestReceiver
import com.yasinkacmaz.backgroundlocationtracker.receiver.request.servicestatus.ServiceStatusRequestCallback
import com.yasinkacmaz.backgroundlocationtracker.receiver.request.servicestatus.ServiceStatusRequestReceiver
import com.yasinkacmaz.backgroundlocationtracker.receiver.response.lastlocation.LastLocationResponseReceiver
import com.yasinkacmaz.backgroundlocationtracker.receiver.response.servicestatus.ServiceStatusResponseReceiver
import com.yasinkacmaz.backgroundlocationtracker.utils.extension.isLocationPermissionGranted

class LocationTrackerService : Service(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, ServiceStatusRequestCallback,
    LastLocationRequestCallback {

    private var googleApiClient: GoogleApiClient? = null
    private var locationRequest: LocationRequest? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null

    private var locationResultCallback: LocationResultCallback? = null
    private var lastLocation: Location? = null
    private lateinit var serviceStatusRequestReceiver: ServiceStatusRequestReceiver
    private lateinit var lastLocationRequestReceiver: LastLocationRequestReceiver

    override fun onCreate() {
        Log.v("yaso", "create")
        super.onCreate()
        serviceStatusRequestReceiver = ServiceStatusRequestReceiver(this)
        lastLocationRequestReceiver = LastLocationRequestReceiver(this)

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            showNotificationAndStartForegroundService()
//        }

        init()
    }

    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.v("yaso","start command")
        registerServiceStatusReceiver()
        registerLastLocationRequestReceiver()
        sendServiceStatusResponse()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        if (!isLocationPermissionGranted()) {
            return START_STICKY
        }

        fusedLocationProviderClient?.lastLocation?.addOnSuccessListener { location ->
            location?.let {
                lastLocation = it
                sendLastLocationTimeIntent()
            }
        }

        googleApiClient?.let {
            if (it.isConnected) {
                createLocationRequest()
            } else {
                buildGoogleApiClient()
            }
        }

        return START_STICKY
    }

    private fun registerServiceStatusReceiver() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                serviceStatusRequestReceiver,
                ServiceStatusRequestReceiver.getIntentFilter()
            )
    }

    private fun registerLastLocationRequestReceiver() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                lastLocationRequestReceiver,
                LastLocationRequestReceiver.getIntentFilter()
            )
    }

    override fun onBind(intent: Intent): IBinder? = null

    override fun onLowMemory() {
        destroy()
        super.onLowMemory()
        restartService()
    }

    override fun onDestroy() {
        Log.v("yaso", "destroy")
        destroy()
        restartService()
    }

    private fun destroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(serviceStatusRequestReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(lastLocationRequestReceiver)
        fusedLocationProviderClient?.removeLocationUpdates(locationResultCallback)
        googleApiClient?.disconnect()
        locationResultCallback = null
    }

    private fun restartService() {
        startService(Intent(this, LocationTrackerService::class.java))
    }

    override fun onConnected(p0: Bundle?) {
        createLocationRequest()
    }

    override fun onConnectionSuspended(i: Int) {
        buildGoogleApiClient()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        buildGoogleApiClient()
    }

    private fun init() {
        buildGoogleApiClient()
    }

    @Synchronized
    private fun buildGoogleApiClient() {
        googleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        googleApiClient?.connect()
    }

    private fun createLocationRequest() {
        locationResultCallback = LocationResultCallback()

        locationRequest = LocationRequest.create().apply {
            interval = 5000
            fastestInterval = 3000
            priority = LocationRequest.PRIORITY_NO_POWER
            smallestDisplacement = 2.0f
        }
        requestLocationUpdates()
    }

    @SuppressLint("MissingPermission")
    fun requestLocationUpdates() {
        if (isLocationPermissionGranted()) {
            fusedLocationProviderClient?.requestLocationUpdates(
                locationRequest,
                locationResultCallback,
                Looper.myLooper()
            )
        }
    }

    //Start Foreground Service and Show Notification to user for Android O and higher Version
    private fun showNotificationAndStartForegroundService() {
        val pendingIntent = PendingIntent.getActivity(
            this,
            REQUEST_CODE, Intent(this, MainActivity::class.java),
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val channelId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(CHANNEL_ID, CHANNEL_NAME)
        } else {
            ""
        }

        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_location_on_black_24dp)
            .setContentTitle(getString(R.string.app_name))
            .setAutoCancel(false)
            .setPriority(PRIORITY_MAX)
            .setCategory(Notification.CATEGORY_SERVICE)
            .setContentIntent(pendingIntent)

        startForeground(NOTIFICATION_ID, notificationBuilder.build())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val channel =
            NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH).apply {
                lightColor = Color.RED
                lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            }
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(channel)
        return channelId
    }

    inner class LocationResultCallback : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult!!.lastLocation?.let {
                lastLocation = it
            }
        }
    }

    override fun onServiceStatusRequest() {
        sendServiceStatusResponse()
    }

    private fun sendServiceStatusResponse() {
        LocalBroadcastManager.getInstance(this)
            .sendBroadcast(ServiceStatusResponseReceiver.getIntent())
    }

    override fun onLastLocationRequest() {
        sendLastLocationTimeIntent()
    }

    private fun sendLastLocationTimeIntent() {
        lastLocation?.let {
            LocalBroadcastManager.getInstance(this@LocationTrackerService)
                .sendBroadcast(LastLocationResponseReceiver.getIntent(it.time))
        }
    }

    companion object {
        private const val NOTIFICATION_ID = 1337
        private const val CHANNEL_ID = BuildConfig.APPLICATION_ID + "_channel_id"
        private const val CHANNEL_NAME = BuildConfig.APPLICATION_ID + "_channel_name"
        private const val REQUEST_CODE = 1

        fun start(context: Context) {
            context.startService(Intent(context, LocationTrackerService::class.java))
        }
    }
}
