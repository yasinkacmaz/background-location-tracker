package com.yasinkacmaz.backgroundlocationtracker.receiver.response.servicestatus

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

class ServiceStatusResponseReceiver(
    private val callback: ServiceStatusResponseCallback
) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent ?: return

        if (intent.action == ACTION_SERVICE_STATUS_RESPONSE) {
            callback.onServiceStatusResponse()
        }
    }

    companion object {
        private const val ACTION_SERVICE_STATUS_RESPONSE = "serviceStatusResponse"

        fun getIntentFilter(): IntentFilter = IntentFilter(ACTION_SERVICE_STATUS_RESPONSE)

        fun getIntent(): Intent = Intent(ACTION_SERVICE_STATUS_RESPONSE)
    }
}
