package com.yasinkacmaz.backgroundlocationtracker.receiver.request.servicestatus

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

class ServiceStatusRequestReceiver(
    private val callback: ServiceStatusRequestCallback
) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent ?: return

        if (intent.action == ACTION_SERVICE_STATUS_REQUEST) {
            callback.onServiceStatusRequest()
        }
    }

    companion object {
        private const val ACTION_SERVICE_STATUS_REQUEST = "serviceStatusRequest"

        fun getIntentFilter(): IntentFilter = IntentFilter(ACTION_SERVICE_STATUS_REQUEST)

        fun getIntent(): Intent = Intent(ACTION_SERVICE_STATUS_REQUEST)
    }
}
