package com.yasinkacmaz.backgroundlocationtracker.receiver.request.lastlocation

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

class LastLocationRequestReceiver(
    private val callback: LastLocationRequestCallback
) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent ?: return

        if (intent.action == ACTION_LAST_LOCATION_REQUEST) {
            callback.onLastLocationRequest()
        }
    }

    companion object {
        private const val ACTION_LAST_LOCATION_REQUEST = "lastLocationRequest"

        fun getIntentFilter(): IntentFilter = IntentFilter(ACTION_LAST_LOCATION_REQUEST)

        fun getIntent(): Intent = Intent(ACTION_LAST_LOCATION_REQUEST)
    }
}
