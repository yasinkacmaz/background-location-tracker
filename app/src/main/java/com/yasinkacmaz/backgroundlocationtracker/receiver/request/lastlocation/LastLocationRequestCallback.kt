package com.yasinkacmaz.backgroundlocationtracker.receiver.request.lastlocation

interface LastLocationRequestCallback {
    fun onLastLocationRequest()
}
