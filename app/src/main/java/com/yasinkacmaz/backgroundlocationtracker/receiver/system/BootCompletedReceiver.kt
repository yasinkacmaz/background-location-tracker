package com.yasinkacmaz.backgroundlocationtracker.receiver.system

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.yasinkacmaz.backgroundlocationtracker.LocationTrackerService

class BootCompletedReceiver : BroadcastReceiver() {
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context?, intent: Intent?) {
        LocationTrackerService.start(context!!)
    }
}
