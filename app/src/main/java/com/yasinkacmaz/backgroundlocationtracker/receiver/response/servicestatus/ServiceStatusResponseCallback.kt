package com.yasinkacmaz.backgroundlocationtracker.receiver.response.servicestatus

interface ServiceStatusResponseCallback {
    fun onServiceStatusResponse()
}
