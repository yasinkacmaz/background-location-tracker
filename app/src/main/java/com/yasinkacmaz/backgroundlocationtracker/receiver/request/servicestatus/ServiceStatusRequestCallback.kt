package com.yasinkacmaz.backgroundlocationtracker.receiver.request.servicestatus

interface ServiceStatusRequestCallback {
    fun onServiceStatusRequest()
}
