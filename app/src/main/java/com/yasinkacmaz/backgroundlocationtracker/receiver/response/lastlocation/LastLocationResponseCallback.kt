package com.yasinkacmaz.backgroundlocationtracker.receiver.response.lastlocation

interface LastLocationResponseCallback {
    fun onLastLocation(timeStamp: Long)
}
