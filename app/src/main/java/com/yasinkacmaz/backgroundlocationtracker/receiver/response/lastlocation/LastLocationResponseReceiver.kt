package com.yasinkacmaz.backgroundlocationtracker.receiver.response.lastlocation

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

class LastLocationResponseReceiver(
    private val callback: LastLocationResponseCallback
) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent ?: return

        if (intent.action == ACTION_LAST_LOCATION_RESPONSE) {
            callback.onLastLocation(getTimeStamp(intent))
        }
    }

    companion object {
        private const val ACTION_LAST_LOCATION_RESPONSE = "lastLocationResponse"
        private const val KEY_LAST_LOCATION_TIMESTAMP = "lastLocationTimeStamp"

        fun getIntentFilter(): IntentFilter = IntentFilter(ACTION_LAST_LOCATION_RESPONSE)

        fun getIntent(timeStamp: Long): Intent = Intent(ACTION_LAST_LOCATION_RESPONSE).apply {
            putExtra(KEY_LAST_LOCATION_TIMESTAMP, timeStamp)
        }

        fun getTimeStamp(intent: Intent): Long = intent.getLongExtra(KEY_LAST_LOCATION_TIMESTAMP, 0)
    }
}
